#!/usr/bin/env bash

################################################################################
# Disk Usage Message of the Day (motd)
#
# This script generates a visually appealing Message of the Day (motd) that
# displays disk usage information for the system. It utilizes the `df` and
# `mapfile` commands to retrieve disk usage data and formats the output with
# color-coded progress bars and clear drive information.
#
# Usage:
# - Place this script in the `/etc/update-motd.d` directory.
# - Ensure the script is executable (e.g., `chmod +x <script_name>`).
# - The script will be automatically executed, and the output will be displayed
#   as part of the Message of the Day (motd) when logging in to the system.
#
# Repository: https://git.chruth.xyz/dotfiles-server/
#
################################################################################

# Check if required commands are available
if ! command -v df &>/dev/null || ! command -v mapfile &>/dev/null; then
  echo "Required commands (df and mapfile) not found. Please make sure they are installed." >&2
  exit 1
fi

# Define color codes
color_reset='\e[0m'
color_red='\e[1;31m'
color_green='\e[1;32m'
color_blue='\e[1;34m'

# Retrieve disk usage, excluding specific file systems
mapfile -t dfs < <(df --si --exclude-type zfs \
  --exclude-type fuse.mergerfs --exclude-type fuse.rclone \
  --exclude-type overlay --exclude-type squashfs \
  --exclude-type tmpfs --exclude-type devtmpfs \
  --output=target,pcent,size | tail --lines +2)
printf "\nDisk usage:\n"

# Process disk usage information
for i in "${dfs[@]}"; do
  read -r target usage size <<<"$i"
  usage="${usage%?}"

  # If the usage exceeds the threshold of 90%, the color will be changed to red
  color=$color_green
  if ((usage >= 90)); then
    color=$color_red
  fi

  # Create a colored progress bar to visually represent the disk usage
  used_width=$((usage * 50 / 100))
  bar="[${color}"
  bar+=$(printf '=%.0s' $(seq 1 "$used_width"))
  bar+="${color_reset}"
  bar+=$(printf '=%.0s' $(seq "$((used_width + 1))" "50"))
  bar+="${color_reset}]"

  # Display the disk usage information along with the colored progress bar
  printf "  ${color_blue}%-29s %+3s%%${color_reset} used out of ${color_blue}%+4s${color_reset}\n" "$target" "$usage" "$size"
  echo -e "  $bar\e[0m"
done